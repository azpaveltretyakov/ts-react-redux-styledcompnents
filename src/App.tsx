import React from 'react';
import { Counter } from './features/counter/Counter';
import './App.css';
import tw, { styled } from 'twin.macro';

function App() {
  return (
    <div className="App">
      <Box>Styled Box content</Box>

      <header className="App-header">
        <Counter />
      </header>
    </div>
  );
}

const Box = styled.div`
  ${tw`border-dashed border-2 border-gray-400 p-10`};
`;

export default App;
